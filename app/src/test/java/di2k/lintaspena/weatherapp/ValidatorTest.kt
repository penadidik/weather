package di2k.lintaspena.weatherapp

import org.junit.Assert
import org.junit.Test
import di2k.lintaspena.weatherapp.util.getDayOfWeek
import di2k.lintaspena.weatherapp.util.isToday


class ValidatorTest {

    @Test
    fun isTodayTest() {


        var actual = isToday("2018-09-05")
        var expected = true

        Assert.assertEquals(expected, actual)
    }

    @Test
    fun testDayOfWeek() {
        var dayOfWeek = getDayOfWeek("2018-09-05").toLowerCase()
        Assert.assertEquals("wednesday",dayOfWeek)
        Assert.assertNotNull(dayOfWeek)
//        Assert.assertSame("wednesday",dayOfWeek)

    }
}