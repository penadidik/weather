package di2k.lintaspena.weatherapp.modul

import dagger.Module
import dagger.Provides
import di2k.lintaspena.weatherapp.util.SchedulersUtil

@Module
class MainModule {

    @Provides fun getSchedulers() : SchedulersUtil{
        return SchedulersUtil()
    }
}