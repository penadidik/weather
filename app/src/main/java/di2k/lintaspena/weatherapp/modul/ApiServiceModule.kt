package di2k.lintaspena.weatherapp.modul

import dagger.Module
import dagger.Provides
import di2k.lintaspena.weatherapp.network.NetworkHandler
import di2k.lintaspena.weatherapp.network.WeatherApiService
import javax.inject.Named

@Module
class ApiServiceModule {

    @Provides @Named("apiservice")fun getApiService() : WeatherApiService {
        return NetworkHandler.getApiService()
    }
}