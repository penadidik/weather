package di2k.lintaspena.weatherapp.modul

import dagger.Module
import dagger.Provides
import di2k.lintaspena.weatherapp.network.WeatherApiService
import di2k.lintaspena.weatherapp.data.usecases.CurrentWeatherUsecase
import di2k.lintaspena.weatherapp.data.usecases.FetchWeatherInfoUsecase
import di2k.lintaspena.weatherapp.data.usecases.WeatherForecasteUsecase
import javax.inject.Named

@Module(includes = arrayOf(ApiServiceModule::class))
class UsecaseModule {

    @Provides
    fun getFetchWeatherInfoUsecase(@Named("apiservice")apiServide : WeatherApiService) : FetchWeatherInfoUsecase {
        return FetchWeatherInfoUsecase(getWeatherForecasteUsecase(apiServide),getCurrentWeatherUsecase(apiServide))
    }

    @Provides
    fun getWeatherForecasteUsecase(apiServide : WeatherApiService) : WeatherForecasteUsecase {
        return WeatherForecasteUsecase(apiServide)
    }

    @Provides
    fun getCurrentWeatherUsecase(apiServide : WeatherApiService) : CurrentWeatherUsecase {
        return CurrentWeatherUsecase(apiServide)
    }

}