package di2k.lintaspena.weatherapp.modul

import dagger.Component
import di2k.lintaspena.weatherapp.ui.weather_info.activities.MainActivity
import di2k.lintaspena.weatherapp.ui.weather_info.presenter.MainPresenter

@Component (modules = arrayOf(MainModule::class,UsecaseModule::class))
interface MainComponent {

    fun inject(presenter : MainPresenter)
    fun inject(activity : MainActivity)
}