package di2k.lintaspena.weatherapp

import android.app.Application
import android.content.Context
import di2k.lintaspena.weatherapp.network.NetworkHandler

lateinit var appContext : Context

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        appContext = this
        NetworkHandler.init()
    }



}