package di2k.lintaspena.weatherapp.data.usecases

import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import di2k.lintaspena.weatherapp.data.response.CurrentWeatherResponse
import di2k.lintaspena.weatherapp.data.response.ForecastWeatherResponse
import di2k.lintaspena.weatherapp.data.response.WeatherInfo
import di2k.lintaspena.weatherapp.util.isToday
import javax.inject.Inject

open class FetchWeatherInfoUsecase @Inject constructor(var forecasteUsecase: WeatherForecasteUsecase, var currentWeatherUsecase: CurrentWeatherUsecase) : Interactor<WeatherInfo> {

    lateinit var cityName: String

    override fun execute(): Flowable<WeatherInfo> {

        forecasteUsecase.cityName = cityName
        currentWeatherUsecase.cityName = cityName

        var flowable = currentWeatherUsecase.execute().zipWith(forecasteUsecase.execute(),
                object : BiFunction<CurrentWeatherResponse, ForecastWeatherResponse, WeatherInfo> {
                    override fun apply(t1: CurrentWeatherResponse, t2: ForecastWeatherResponse): WeatherInfo {
                        return WeatherInfo(t1, t2.forecast.forecastday.filter { !isToday(it.date) })
                    }
                })


        return flowable
    }


}