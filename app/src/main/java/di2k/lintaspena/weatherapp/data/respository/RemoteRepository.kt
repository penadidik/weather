package di2k.lintaspena.weatherapp.data.respository

import io.reactivex.Flowable
import di2k.lintaspena.weatherapp.network.WeatherApiService
import di2k.lintaspena.weatherapp.data.response.CurrentWeatherResponse
import di2k.lintaspena.weatherapp.data.response.ForecastWeatherResponse

class RemoteRepository constructor(var apiService: WeatherApiService) {

    fun getCurrentWeather(): Flowable<CurrentWeatherResponse> {
        return apiService.getCurrentWeather()
    }

    fun getWeatherforecast(): Flowable<ForecastWeatherResponse> {
        return apiService.getWeatherForecast()
    }
}