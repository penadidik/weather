package di2k.lintaspena.weatherapp.data.usecases

import io.reactivex.Flowable
import di2k.lintaspena.weatherapp.network.WeatherApiService
import di2k.lintaspena.weatherapp.data.response.CurrentWeatherResponse


open class CurrentWeatherUsecase(var apiService: WeatherApiService) : Interactor<CurrentWeatherResponse> {
    lateinit var cityName : String
    override fun execute(): Flowable<CurrentWeatherResponse> {
        return apiService.getCurrentWeather(cityName = cityName)
    }
}
