package di2k.lintaspena.weatherapp.data.usecases

import io.reactivex.Flowable
import di2k.lintaspena.weatherapp.network.WeatherApiService
import di2k.lintaspena.weatherapp.data.response.ForecastWeatherResponse

open class WeatherForecasteUsecase(var apiService: WeatherApiService) : Interactor<ForecastWeatherResponse> {
    lateinit var cityName : String
    override fun execute(): Flowable<ForecastWeatherResponse> {
        return apiService.getWeatherForecast(cityName = cityName)
    }
}