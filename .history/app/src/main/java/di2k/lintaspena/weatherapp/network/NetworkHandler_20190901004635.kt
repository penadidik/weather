package di2k.lintaspena.weatherapp.network

import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object NetworkHandler {

    val BASE_URL = "http://api.apixu.com/"
    val KEY : String = "eb2a0633229b456ba6093557151106"
    val CITY = "Jakarta"
    val DAYS = "10"

    lateinit private var retrofit: Retrofit

    fun init() {

        val okHttpClient = OkHttpClient().newBuilder().connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS).build()

        retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient).build()
    }


    fun getApiService(): WeatherApiService {
        return retrofit.create(WeatherApiService::class.java)
    }


}